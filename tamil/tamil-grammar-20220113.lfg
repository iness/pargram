"-*- mode: lfg; Encoding: utf-8 -*-"

PARGRAM  Tamil   CONFIG (1.0)
  ROOTCAT   ROOT.
  CHARACTERENCODING utf-8.
  FILES  	tamil.morph.lfg
		tamil.lex.particles.lfg
		tamil.lex.verbs.lfg 
		tamil.lex.nouns.lfg 
		tamil.templates.lfg
		tamil.common.features.lfg.
  LEXENTRIES   (PARGRAM Tamil).
  RULES   (PARGRAM Tamil).
  TEMPLATES   (PARGRAM Tamil).
  GOVERNABLERELATIONS    SUBJ OBJ OBJ-TH  OBJ-TO OBJ-GO COMP PREDLINK XCOMP OBL OBL-AG OBL-COMPAR OBL-LOC.
  SEMANTICFUNCTIONS    ADJUNCT  XADJUNCT APP MOD TOPIC FOCUS CORR.
  NONDISTRIBUTIVES    DOMAIN NUM PERS GEND  CASE COORD-FORM COORD-LEVEL ADJUNCT.
  EPSILON   e.
  OPTIMALITYORDER     NOGOOD.

----

PARGRAM	Tamil	RULES (1.0)

   ROOT --> (INTJP) {S|Simp|Scop|Stop|Snosub|Spolint|Spass} (INTJP).  "See the notes below"
   
   INTJP --> 	(INTJ)(EXCL)(COMMA)(INTJ)(EXCL)(COMMA)(INTJ)(EXCL)(COMMA).

   Simp --> 	e: (^ SUBJ PRED) = 'pro' 
	       		(^ SUBJ PERS) = 2;
 	       		NP: (^ OBJ)=!;
 	       		ADV*: ! $ (^ ADJUNCT);
 	       		V : ^=!;
 	       		(EXCL).

  "Clause-type - pol-int = yes or no, 
   but what will happen if it is followed by a decl clause? will it be still pol-int clause?"

   Spolint --> 	PolIntMarker
 	       		(EXCL)(COMMA)(PERIOD)
				{S|Simp|Snosub}.

   "Simp and Snosub can be joined, need to check, pers=2 need to be handled in Simp"

   

   Scop --> 	{NP: @SUBJ | NP | ADVQ },(NP),
 	  	 		{NP: @PREDLINK {(! CASE)=nom | (! CASE)=dat}
				|AP: @PREDLINK
				|AdjecPP: @PREDLINK
				|ADV: @PREDLINK}
				(VC). 

S --> 	    {(NP: (^ SUBJ)=! 
	      		{(! CASE)=nom | (! CASE)=dat})|
				  e: (^ SUBJ PRED) = 'pro'},
 	   		(NP: (^ OBJ)=! 
	      		{(! CASE)=nom | (! CASE)=acc}),
			(NP: (^ OBL)=! 
	      		(! CASE)=loc),
 	 		(NP: (^ OBJ-TH)=! 
	      		(! CASE)=dat),
 	 		NP*: ! $ (^ ADJUNCT) 
	     		{(! PTYPE) =c sem  
				 | (! CONJ-TYPE) =c um 
				 | (! CASE) = loc 
				 | (! CASE) = nom},
	      	(CP: (^ COMP) = !),  "See the -note on XCOMP- for XCOMP"
			{VC | VCpass},
			{(PERIOD)|(QUESTION)}.

   NP --> (DEM)(NUM)(MODN)(AP)(DquantP)(AdjecPP)(POSSPRON)
		{ (N: ^=!)
		| (NConj: ^=!   {(! CONJ-TYPE) =c um | (! FOCUS-FORM) =c um })  "NConj - N with -um marker"
		| (QP: ^=!) 
		| (QPRON: ^=!) 
		| (PRON: ^=!)}(QUANT : ^=!). 

	MODN --> ({N*: ! $ (^ ADJUNCT) 
   			{(! CASE) = loc 
			   | (! CASE) = nom
			   | (! CASE) = gen 
			   | (! CASE) = acc 
			   |(! CASE) = dat} 
			| (POSSPRON)}).

   CP --> {S|Spass|Snosub|Scop} C.

   AP --> A*: ! $ (^ ADJUNCT). "Adjectival pharse"

   DquantP --> Dquant*: ! $ (^ ADJUNCT).

   AdjecPP --> AdjP*: ! $ (^ ADJUNCT). "Adjectivial Participle Pharse, where a verb plays the role of adjectival"

   VC -->	VPart*: ! $ (^ ADJUNCT)    "See the - Note on verbal conjunctions - "
			{(! VFORM) =c vpart 
			|(! CONJ-TYPE) =c  um},
			VInf*: ! $ (^ ADJUNCT)
			{(! VFORM) =c inf 
			|(! CONJ-TYPE) =c um },
   			ADV*: ! $ (^ ADJUNCT),  	
			{V: ^=!
			|Vmain: @VV-ANNOTATION;
 	  	 	Vlight
			|Vmain, Vaux}.

   VCpass -->	VPart*: ! $ (^ ADJUNCT)    "See the - Note on verbal conjunctions - "
				{(! VFORM) =c vpart 
					|(! CONJ-TYPE) =c  um},
				VInf*: ! $ (^ ADJUNCT)
				{(! VFORM) =c inf 
					|(! CONJ-TYPE) =c um },
   				ADV*: ! $ (^ ADJUNCT),  	
				Vpass: ^=!.

"The following lines are for Coordination - until end-coordination"

METARULEMACRO(_CAT _BASECAT _RHS) =
 	  { _RHS "don't do anything"
 	   |"NP coordination"
 	    e: _CAT $ { NP N } "I have added NConj to handled words with -um"
	       @PUSHUP;
 	    @(NPCOORD _CAT)
 	   |"all other coordination EXCEPT for..."
 	    e: _CAT ~$ { NP N } "I have added NConj to handled words with -um"
	       @PUSHUP;
 	    @(SCCOORD _CAT)
 	  | "AP coordination --- last adjective agrees with the following noun"
 	    e: _CAT $ {AP}
	       @PUSHUP;
 	    @(APCOORD _CAT)
 	   |"surrounding square parentheses for debugging"
 	    L-SQ
 	    _CAT: @PUSHUP;
 	    R-SQ}.

NPCOORD(_CAT) = "NP coordination"
 	  e: (^ COORD-LEVEL) = NP
	     { (^ PERS)=c 1 "constraints to deal with verb assigning PERS"
	       (! PERS)=c 1
	      |(^ PERS)=c 2
	       (! PERS)=c 2
	      |(^ PERS)=c 3}; "for English grammar"
 	  _CAT: @NPCONJUNCT; "first conjunct"
 	  [COMMA: @(OT-MARK GoodPunct); "extra conjuncts with commas"
 	   _CAT: @NPCONJUNCT
		 ]*
 	  {COMMA|CONJand|CONJor}   "Could not guess a way to add NConj here as -um is always suffixed, but, works with mattum"
	   "conjunction itself"
 	  _CAT: @NPCONJUNCT
		(^ GEND) = (! GEND).
		

   SCCOORD(_CAT) = "same category coordination; not used for NPs"
 	  _CAT: @IN-SET "first conjunct";
 	  [COMMA: @(OT-MARK GoodPunct); "extra conjuncts with commas"
 	   _CAT: @IN-SET
		 ]*
 	  (COMMA) "optional comma"
 	  {CONJand|CONJor} "conjunction itself"
 	  _CAT: @IN-SET "last conjunct".

   APCOORD(_CAT) =
	  _CAT: @IN-SET ;
	  [COMMA: @(OT-MARK GoodPunct);
	   _CAT: @IN-SET ]*
	  (COMMA)
	  {CONJand|CONJor}
	  APconj:@IN-SET
		 (^GEND) = (!GEND)
		 (^NUM) = (!NUM). 

"end of coordination"


"Need to try this topicalisation, got this piece from the XLE course"
   Stop --> NP: (^ TOPIC) = ! 
	 (^ {XCOMP|COMP}* {OBJ|OBJ-TO})=!;
	NP: (^ SUBJ) = ! ;
	VP.

"
=========================
Notes:
"

"   
(INTJ) {S|Simp|Scop|Stop|Snosub|Spolint}.

Simp - Impreratives, Scop - Copula constructions, Stop - Topicalisation, Snosub - null subject constructions

We decided to use the term INTERJECTION for அம்மா / தம்பி / ஐயோ - basically for the terms which just appears infront of a sentence or clause

"



"There are three SVless patterns in the text-Modern Tamil Grammar - Section 3.0:
Nom + Nom கமல் வக்கீல் (Kamal (is) a lawyer)
Dat +Nom கமலுக்கு பசி (Kamal (is) hungry)
Nom + Dat வீடு கமலுக்கு (House (is for) Kamal)"

" In Tamil Pron, Demonstrative and numarative is the order to be followed: தொடரியல் மாற்றிலக்கண அணுகுமுறை - அரங்கன்"

"-Note on XCOMP-
I did this 1st, 
({CP: (^ COMP) = ! | VInfP:  (^ XCOMP) = !  (^ VFORM) = inf})

VInfP --> 	(NP)
			VInf.

but later also thought that why cant XCOMP cant be considered as a modifier / Adjunct. However, need to revisit.
Specially, when there is a sandhi at the end of VInf, you cannot move it back and forth and it has to be considered as a modifer / together with the finite verb"

"- Note on verbal conjunctions - 
In Tamil -and- coordination can be achieved through mattum and clitic-um
-um can be added to nouns, verbal participles, infinitives, postpositions 
and various clause types also take -um. However, so far only nouns, vpart and inf are handled here - 11.2019

coordinated subject-verb agreement is not handled yet

In Tamil -or- coordination can be achieved through illayaanaal, allathu, -oo, -aavathu, -aa, now only 1st two are added. Others will be similar to -um. However, subj-verb agreement need to checked
"

"
=========================
"

----
